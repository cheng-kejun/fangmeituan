import { createRouter, createWebHashHistory } from "vue-router";

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            component: ()=>import('../pages/home/Home')
        },
        {
            path: '/cart',
            component: ()=>import('../pages/cart/Cart')
        },
        {
            path: '/order',
            component: ()=>import('../pages/order/Order')
        },
        {
            path: '/mine',
            component: ()=>import('../pages/mine/Mine')
        },
        {
            path: '/store',
            component: ()=>import('../pages/store/MyStore')
        },
    ]
})

export default router;